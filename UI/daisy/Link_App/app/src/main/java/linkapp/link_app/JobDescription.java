package linkapp.link_app;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class JobDescription extends AppCompatActivity {
private EditText phone;
CheckBox laundry,babySitting,dishWashing,generalCleaning,cooking,gardening;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_description);
        this.phone=(EditText)findViewById(R.id.phoneEditText);
        laundry=(CheckBox)findViewById(R.id.chkLaundry);
        babySitting=(CheckBox)findViewById(R.id.chkBabySitting);
        dishWashing=(CheckBox)findViewById(R.id.chkDishWashing);
        generalCleaning=(CheckBox)findViewById(R.id.chkGeneralCleaning);
        cooking=(CheckBox)findViewById(R.id.chkCooking);
        gardening=(CheckBox)findViewById(R.id.chkGardening);

        Button btnDone=(Button)findViewById(R.id.btnDone);
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String result="Selected jobs";
                if (laundry.isChecked()){
                    result+="\nLaundry";
                }

                if (babySitting.isChecked()){
                    result+="\nBaby sitting";
                }

                if (dishWashing.isChecked()){
                    result+="\nDish washing";
                }

                if (generalCleaning.isChecked()){
                    result+="\nGeneral cleaning";
                }

                if (cooking.isChecked()){
                    result+="\nCooking";
                }

                if (gardening.isChecked()){
                    result+="\nGardening";
                }
                Toast.makeText(getApplicationContext(),result,Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void onCheckboxClicked(View view){
        boolean checked=((CheckBox)view).isChecked();
        String str="";
        //check which checkbox was clicked
        switch (view.getId()){
            case R.id.chkLaundry:
                str=checked? "Laundry selected":"Laundry deselected";
                break;

            case R.id.chkBabySitting:
                str=checked?"Baby sitting selected":"Baby sitting deselected";
                break;

            case R.id.chkDishWashing:
                str=checked?"Dish washing selected":"Dish washing deselected";
                break;

            case R.id.chkGeneralCleaning:
                str=checked?"General cleaning selected":"General cleaning deselected";
                break;

            case R.id.chkCooking:
                str=checked?"Cooking selected":"Cooking deselected";
                break;
        }
        Toast.makeText(getApplicationContext(),str,Toast.LENGTH_SHORT).show();
    }




    public void btnDone(View v){
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS)!= PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.SEND_SMS},0);

        SmsManager msg=SmsManager.getDefault();
        msg.sendTextMessage(
                this.phone.getText().toString(),
                null, "Thanks you for ,,,,",null,null);

    }

}
