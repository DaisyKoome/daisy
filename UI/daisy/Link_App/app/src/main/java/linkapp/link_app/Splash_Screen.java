package linkapp.link_app;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

public class Splash_Screen extends AppCompatActivity {

    private final int SPLASH_DISPLAY_LENGTH=7000;
    Handler handler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

    new Handler().postDelayed(new Runnable() {
        @Override
        public void run() {
            Intent mainIntent=new Intent(Splash_Screen.this,Login_Sign_Up.class);
            Splash_Screen.this.startActivity(mainIntent);
            Splash_Screen.this.finish();
        }
    },SPLASH_DISPLAY_LENGTH);





    }

//    public void btnWelcome(View v){
//        Intent proceed=new Intent(this,Login_Sign_Up.class);
//        startActivity(proceed);
//    }


}
