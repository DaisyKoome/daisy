package linkapp.link_app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class Login_Sign_Up extends AppCompatActivity {
    TextView idEditText;
    TextView usernameEditText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_sign_up);

    }

    public void btnLogIn(View v){
        Intent logIn=new Intent(this,JobDescription.class);
        startActivity(logIn);
        idEditText = findViewById(R.id.idEditText);
        usernameEditText=findViewById(R.id.userNameEditText);


      //  String id = idEditText.getText().toString();
      //  Toast.makeText(this, id, Toast.LENGTH_SHORT).show();

        String username=usernameEditText.getText().toString();
        Toast.makeText(this,username,Toast.LENGTH_SHORT).show();

    }

    public void btnProceed(View v){
        Intent proceed= new Intent(this,Register.class);
        startActivity(proceed);
    }
}
