package linkapp.link_app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.util.Log;

public class SmsReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        SmsMessage[]msgs= Telephony.Sms.Intents.getMessagesFromIntent(intent);
        if (msgs!=null && msgs.length>0){
            for (SmsMessage m:msgs){
                Log.i("Tag",m.getMessageBody());
            }
        }
    }
}
