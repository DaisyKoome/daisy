package test1.scit.ac.ke.newapp;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.EditText;

public class  MessageActivity extends AppCompatActivity {
private EditText phone;
private EditText msg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        this.phone=(EditText)findViewById(R.id.phoneEditText);
        this.msg=(EditText)findViewById(R.id.messageEditText);
    }
    public void btnBack(View v){
        Intent back=new Intent(this,MainActivity.class);
        startActivity(back);
    }
    public void btnSend(View v){

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS)!=
                PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.SEND_SMS}, 0);

        SmsManager msg=SmsManager.getDefault();
        msg.sendTextMessage(this.phone.getText().toString(),null,this.msg.getText().toString(),null,null);

    }

}
