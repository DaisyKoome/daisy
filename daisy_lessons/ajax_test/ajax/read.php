<?php  
	
  	// Connect to db
  	$host = "localhost"; 
  	$user = "root";
  	$password = ""; 
  	$database = "php_sql"; 
  	$con = new mysqli($host, $user, $password, $database);
  	 
  	// Check connection
  	if ($con->connect_error) {
  	    die("Connection failed: " . $con->connect_error);
  	}
   
  
  

  	$query = "SELECT * FROM products";

  	if (!$result = mysqli_query($con, $query)) {
        exit(mysqli_error($con));
    }

    // if query results contains rows then featch those rows 
    if(mysqli_num_rows($result) > 0)
    {
      // $numbering = 1;
        $data = '
        <table class="table table-bordered">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Quantity</th>
            <th scope="col">Brand</th>
            <th scope="col">Price</th>
          </tr>
        </thead>
        <tbody>
        ';
      while($row = mysqli_fetch_assoc($result))
      {
        // $numbering += 1;
        $data .= '
      <tr>
        <th scope="row"></th>
        <td>'.$row["name"].'</td>
        <td>'.$row["qty"].'</td>
        <td>'.$row["brand"].'</td>
        <td>'.$row["price"].'</td>
      </tr>
      ';
      }
      $data .= '
         </tbody>
    	</table>
      ';
    }
    else
    {
      // records now found 
      $data = '<div class="alert alert-primary" role="alert">No data in database</div>';
    }

    //$data .= '</table>';

    echo $data;
?>