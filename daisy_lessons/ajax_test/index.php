<!DOCTYPE html>
<html>
<head>
	<!-- title -->
	<title>jquery tests</title>

	<!-- metas -->
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- css links -->
	<!-- <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css"> -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css" >
	

	<!-- js scripts -->
	<script type="text/javascript" src="assets/js/jquery.js"></script>
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script> -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>

	<!-- custom script -->
	<script type="text/javascript" src="assets/js/script.js"></script>

</head>
<body>
	<div class="container">
		<!-- row 1 -->
		<div class="row">
			<div style="background-color:#E7174B;width:200px;height: 200px;"></div>
		</div>

		<!-- row 2 -->
		<div class="row">
			<input id="ip_2"></input>
		</div>

		<!-- row 2 -->
		<div class="row">
			<div id="op">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
		</div>

		<!-- row 3 -->
		<div class="row">
			<button id="ip" class="btn btn-outline-success">Click</button>
		</div>
	</div>
</body>
</html>