<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress2' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'cb U?]*aWR5?vr!PBaDXudG1&+EjeZ`0-XvNK0Plv4TWWNKFRQ9LDg&j(twsNWqk' );
define( 'SECURE_AUTH_KEY',  'j3McUi/I`sT:Z w*}a0h|xGLH<O>C!})Jv>@y;O_i!_W:}: YLUh9`g9[PI4V;oG' );
define( 'LOGGED_IN_KEY',    '<m:4ORe=t/tAjzCLLWC,Sma? s/}Ug#n_#o/As)L{*VGJp?_]*BXn<B*WN-+/y?Y' );
define( 'NONCE_KEY',        '5(=[]8RXg4ewA$f =GluRddWyI%c&^/?@kkQg-]C(k`xPy;JOpvSBUdi~ YfUUM_' );
define( 'AUTH_SALT',        'i2}Ni$X7Wl[e$j)2dv=X|YY+^yZ`[eB9Nr(8+GC$P0bTKqdvL41;HX&Ejchch UI' );
define( 'SECURE_AUTH_SALT', 'Z#$|0rj`X_bJVrm>|kg/$%QC%D2(V)?>ZN?0st`Pp(q&ym/.+|mW~oNC<%5`gNwv' );
define( 'LOGGED_IN_SALT',   '5b&!GwE_J7h=2>c<S_DG_z|VB@L-P2c2S+RK8l&n|rE/C|$uz{o=j`$*>V|VZ@&-' );
define( 'NONCE_SALT',       '(@5Pfxo[?&oM1d `bd^VV8So?+d:TSlp$O1H+W>a:Pm2R:vbq;16NbcQLmQo(-aj' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'e_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
