<?php

$connect=mysqli_connect("localhost","root","","e-commerce");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
 }

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Cradle | Shop</title>
        <meta name="description" content="This is the description">
        <link rel="stylesheet" href="../styles.css" />
        <script src="../store.js" async></script>
        <!-- Links -->
  <link rel="stylesheet" type="text/css" href="..\ZuriCSS\ZuriCSS.css"><meta charset="UTF-8">
  <link rel="stylesheet" href="..\Bootstrap_Code\bCSS.css">
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> -->
  <link href="..\ZuriCSS\Font_Pacifico.css" rel="stylesheet"> 
  <link href="..\ZuriCSS\W3CSS.css" rel="stylesheet">
  <link href="..\ZuriCSS\Font_JosefinSans.css" rel="stylesheet"> 
 <!--  <link rel="stylesheet" href="..\ZuriCSS\socailmediaicons.css">   -->
 <style type="text/css">

    .navbar {
  margin-bottom: 0;
  background-color: #2d2d30;
  border: 0;
  font-size: 11px !important;
  letter-spacing: 4px;
  opacity: 0.9;
  padding-bottom: 0;
  height: 70px;
}

/* Add a gray color to all navbar links */
.navbar li a, .navbar .navbar-brand {
  color: #d5d5d5 !important;
}

/* On hover, the links will turn white */
.navbar-nav li a:hover {
  color: #fff !important;
}

/* The active link */
.navbar-nav li.active a {
  color: #fff !important;
  background-color:#29292c !important;
}

/* Remove border color from the collapsible button */
.navbar-default .navbar-toggle {
  border-color: transparent;
}
     /* Dropdown container (hidden by default). Optional: add a lighter background color and some left padding to change the design of the dropdown content */
.dropdown-container {
  display: none;
  background-color: #262626;
  padding-left: 8px;
}

/* Optional: Style the caret down icon */
.fa-caret-down {
  float: right;
  padding-right: 8px;
}

/* Some media queries for responsiveness */
@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}

.fa {
  /*padding: 10px;*/
  font-size: 30px;
  /*width: 50px;*/
  /*text-align: center;*/
  text-decoration: none;
  /*margin: 5px 2px;*/
}


.fa:hover{
    opacity: 0.7;
}
 </style>
    </head>
    <body>
        <div id="header">
    <nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
    
      <span class="navbar-brand" style="font-family: 'Pacifico', cursive; font-size: 50px; ">Zuri Hair Collection</span>
      
    </div>
   
   
</div>
  </div>
</nav>
</div>
<!--  End of navigation bar/header -->



</header>

        

 
  <h3 style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"> To get a receipt, please select the "Add to cart" button for all your shopped items again then scroll down to get your receipt</h3>
  <script>
/* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
  this.classList.toggle("active");
  var dropdownContent = this.nextElementSibling;
  if (dropdownContent.style.display === "block") {
  dropdownContent.style.display = "none";
  } else {
  dropdownContent.style.display = "block";
  }
  });
}
</script>


   
         <section class="container content-section">
          <?php  
          $query="SELECT * FROM beauty where id=4604";
        $result=mysqli_query($connect,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $image=$row['image'];
        $name=$row['name'];
        $price=$row['price'];
        }
        
        ?>

            <h2 class="section-header" style="font-family: Jokerman;" id="Human-hair">Human-hair</h2>
            <div class="shop-items">
                <div class="shop-item">
                    <span class="shop-item-title"><?php echo $name;?></span>
                     <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>
                <?php  
            $query="SELECT * FROM beauty where id=4605";
            $result=mysqli_query($connect,$query);
        
            while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
            $image=$row['image'];
            $name=$row['name'];
            $price=$row['price'];
            }
            
            ?>
                <div class="shop-item">
                    <span class="shop-item-title"><?php echo $name;?></span>
                     <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

            <?php  
            $query="SELECT * FROM beauty where id=4606";
            $result=mysqli_query($connect,$query);
        
            while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
            $image=$row['image'];
            $name=$row['name'];
            $price=$row['price'];
            }
            
            ?>

                <div class="shop-item">
                    <span class="shop-item-title"><?php echo $name;?></span>
                     <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

                <?php  
                $query="SELECT * FROM beauty where id=4607";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>

                <div class="shop-item">
                    <span class="shop-item-title"><?php echo $name;?></span>
                    <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

             <?php  
                $query="SELECT * FROM beauty where id=4608";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>

                <div class="shop-item">
                     <span class="shop-item-title"><?php echo $name;?></span>
                     <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span> 
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

                 <?php  
                $query="SELECT * FROM beauty where id=4609";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>

                <div class="shop-item">
                     <span class="shop-item-title"><?php echo $name;?></span>
                     <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

                 <?php  
                $query="SELECT * FROM beauty where id=4610";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>

                <div class="shop-item">
                    <br><br>
                    <span class="shop-item-title" style="font-family: Jokerman;" id="Curlers, combs & Tongs">Curlers, combs & Tongs<br><?php echo $name;?></span>
                     <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

                <?php  
                $query="SELECT * FROM beauty where id=4611";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>

                <div class="shop-item">
                    <span class="shop-item-title"><br><br><?php echo $name;?></span>
                     <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

                <?php  
                $query="SELECT * FROM beauty where id=4612";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>

                <div class="shop-item">
                    <span class="shop-item-title"><br><br><?php echo $name;?></span>
                     <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button"type="button">ADD TO CART</button>
                    </div>
                </div>

                <?php  
                $query="SELECT * FROM beauty where id=4613";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>

                <div class="shop-item">
                    <span class="shop-item-title"><br><br><?php echo $name;?></span>
                     <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

                <?php  
                $query="SELECT * FROM beauty where id=4614";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>

                <div class="shop-item">
                    <span class="shop-item-title"><br><br><?php echo $name;?></span>
                     <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

                 <?php  
                $query="SELECT * FROM beauty where id=4615";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>

                <div class="shop-item">
                    <span class="shop-item-title"><br><br><?php echo $name;?></span>
                     <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

            </div>
        </section>
        <section class="container content-section">
            <h2 class="section-header" style="font-family: Jokerman;" id="Lipstick_Galore">Lipstick_Galore</h2>

             <?php  
                $query="SELECT * FROM beauty where id=4616";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>

            <div class="shop-items">
                <div class="shop-item">
                   <span class="shop-item-title"><br><br><?php echo $name;?></span>
                   <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

            <?php  
                $query="SELECT * FROM beauty where id=4617";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>
            

                <div class="shop-item">
                    <span class="shop-item-title"><br><br><?php echo $name;?></span>
                    <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>
           

            <?php  
                $query="SELECT * FROM beauty where id=4618";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>
            

                <div class="shop-item">
                    <span class="shop-item-title"><br><br><?php echo $name;?></span>
                     <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>
        

            <?php  
                $query="SELECT * FROM beauty where id=4619";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>
            

                <div class="shop-item">
                    <span class="shop-item-title"><br><br><?php echo $name;?></span>
                   <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

                <?php  
                $query="SELECT * FROM beauty where id=4620";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>
            

                <div class="shop-item">
                    <span class="shop-item-title"><br><br><?php echo $name;?></span>
                   <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>


                <?php  
                $query="SELECT * FROM beauty where id=4621";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>
            

                <div class="shop-item">
                    <span class="shop-item-title"><br><br><?php echo $name;?></span>
                   <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' class='shop-item-image'/>;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

            </div>

        </section>
        <br><br>
        <section class="container content-section">
            <h2 class="section-header" style="font-family: Jokerman">Receipt</h2>
            <div class="cart-row">
                <span class="cart-item cart-header cart-column">ITEM</span>
                <span class="cart-price cart-header cart-column">PRICE</span>
                <span class="cart-quantity cart-header cart-column">QUANTITY</span>
            </div>
            <div class="cart-items">
            </div>
            <div class="cart-total">
                <strong class="cart-total-title">Total</strong>
                <span class="cart-total-price">$0</span>
            </div>
           

        </section>
 </body>
 </html>