<?php

$connect=mysqli_connect("localhost","root","","e-commerce");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
 }

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Cradle | Shop</title>
        <meta name="description" content="This is the description">
        <link rel="stylesheet" href="../styles.css" />
        <script src="../store.js" async></script>
        <!-- Links -->
  <link rel="stylesheet" type="text/css" href="..\ZuriCSS\ZuriCSS.css"><meta charset="UTF-8">
  <link rel="stylesheet" href="..\Bootstrap_Code\bCSS.css">
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> -->
  <link href="..\ZuriCSS\Font_Pacifico.css" rel="stylesheet"> 
  <link href="..\ZuriCSS\W3CSS.css" rel="stylesheet">
  <link href="..\ZuriCSS\Font_JosefinSans.css" rel="stylesheet"> 
 <!--  <link rel="stylesheet" href="..\ZuriCSS\socailmediaicons.css">   -->

 <section class="container content-section">
          <?php  
          $query="SELECT * FROM beauty where id=4573";
        $result=mysqli_query($connect,$query);
        
        while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
        $image=$row['image'];
        $name=$row['name'];
        $price=$row['price'];
        }
        
        ?>

        <div class="shop-items">
                <div class="shop-item">
                    <span class="shop-item-title"><?php echo $name;?></span>
                     <img src="..\ZuriIMAGES\img36.jpg" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>