<?php

$connect=mysqli_connect("localhost","root","","e-commerce");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
 }

?>

<!DOCTYPE html>
<html>
<head>
    <title>Zuri_Hair</title>
    <link rel="stylesheet" type="text/css" href="..\ZuriCSS\ZuriCSS.css">

     <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="..\Bootstrap_Code\bCSS.css">
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> -->
  <link href="..\ZuriCSS\Font_Pacifico.css" rel="stylesheet"> 
  <link href="..\ZuriCSS\W3CSS.css" rel="stylesheet">
  <link rel="stylesheet" href="../styles.css" />
        <script src="../store.js" async></script>
 <!--  <link rel="stylesheet" href="..\ZuriCSS\socailmediaicons.css">   -->

<style type="text/css">
     /* Add a dark background color with a little bit see-through */
.navbar {
  margin-bottom: 0;
  background-color: #2d2d30;
  border: 0;
  font-size: 11px !important;
  letter-spacing: 4px;
  opacity: 0.9;
  padding-bottom: 0;
  height: 70px;
}

/* Add a gray color to all navbar links */
.navbar li a, .navbar .navbar-brand {
  color: #d5d5d5 !important;
}

/* On hover, the links will turn white */
.navbar-nav li a:hover {
  color: #fff !important;
}

/* The active link */
.navbar-nav li.active a {
  color: #fff !important;
  background-color:#29292c !important;
}

/* Remove border color from the collapsible button */
.navbar-default .navbar-toggle {
  border-color: transparent;
}
.sidenav {
  height: 1605px;
  width: 200px;
  position: fixed;
  z-index: 1;
  /*top: 0;
  left: 0;*/
 margin-top: 60px;
  background-color: #111;
  overflow-x: hidden;
  padding-top: 5px;
  margin-top: 5px;
}

/* Style the sidenav links and the dropdown button */
.sidenav a, .dropdown-btn {
  padding: 6px 8px 6px 16px;
  text-decoration: none;
  font-size: 20px;
  color: #818181;
  display: block;
  border: none;
  background: none;
  width: 100%;
  text-align: left;
  cursor: pointer;
  outline: none;
}

/* On mouse-over */
.sidenav a:hover, .dropdown-btn:hover {
  color: #f1f1f1;
}

/* Main content */
.main {
  margin-left: 200px; /* Same as the width of the sidenav */
  font-size: 20px; /* Increased text to enable scrolling */
  padding: 0px 10px;
}

/* Add an active class to the active dropdown button */
.active {
  background-color: #6495ED;
  color: white;
}

/* Dropdown container (hidden by default). Optional: add a lighter background color and some left padding to change the design of the dropdown content */
.dropdown-container {
  display: none;
  background-color: #262626;
  padding-left: 8px;
}

/* Optional: Style the caret down icon */
.fa-caret-down {
  float: right;
  padding-right: 8px;
}

/* Some media queries for responsiveness */
@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}

.fa {
  /*padding: 10px;*/
  font-size: 30px;
  /*width: 50px;*/
  /*text-align: center;*/
  text-decoration: none;
  /*margin: 5px 2px;*/
}


.fa:hover{
    opacity: 0.7;
}

.fa-facebook {
  background: #3B5998;
  color: white;
}

.fa-twitter {
  background: #55ACEE;
  color: white;
}

.fa-instagram {
  background: #125688;
  color: white;
}

.fa-pinterest {
  background: #cb2027;
  color: white;
}

.fa-snapchat-ghost {
  background: #fffc00;
  color: white;
  text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
}

</style> 
</head>
<body>

    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
      <li><a href="Sign_up.html" class="btn-btn-success" style="font-size:12px;">Signing up?</a></li>
      <li><a href="Login_Page.html" class="btn-btn-success" style="font-size:12px;">Logging in?</a></li>
       <li><a href="..\ZuriHTML\Home_real_v2.html">HOME</a></li>
        <li><a href="..\ZuriHTML\Zuri_Hair.html">HAIR</a></li>
        <li><a href="..\ZuriHTML\beauty.php">BEAUTY</a></li>
        <li><a href="..\ZuriHTML\Zuri_HairCare.html">HAIR CARE</a></li>
      </ul>
     
    </div>
   
</div>
  </div>
</nav>
</div>

           <section class="container content-section">
            <h2 class="section-header">Make up brushes</h2>
            <div class="shop-items">
                 <?php  
            $query="SELECT * FROM beauty where id=4569";
            $result=mysqli_query($connect,$query);
        
            while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
            $image=$row['image'];
            $name=$row['name'];
            $price=$row['price'];
            }
            
            ?>
                <div class="shop-item">
                    <span class="shop-item-title"><?php echo $name;?></span>
                     <img src="..\ZuriIMAGES\img36.jpg" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>
                <?php  
            $query="SELECT * FROM beauty where id=4568";
            $result=mysqli_query($connect,$query);
        
            while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
            $image=$row['image'];
            $name=$row['name'];
            $price=$row['price'];
            }
            
            ?>
                <div class="shop-item">
                    <span class="shop-item-title"><?php echo $name;?></span>
                     <img src="..\ZuriIMAGES\img30.jpg" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

            <?php  
            $query="SELECT * FROM beauty where id=4570";
            $result=mysqli_query($connect,$query);
        
            while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
            $image=$row['image'];
            $name=$row['name'];
            $price=$row['price'];
            }
            
            ?>

                <div class="shop-item">
                    <span class="shop-item-title"><?php echo $name;?></span>
                     <img src="..\ZuriIMAGES\img40.jpg" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

                <?php  
                $query="SELECT * FROM beauty where id=4571";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>

                <div class="shop-item">
                    <span class="shop-item-title"><?php echo $name;?></span>
                     <img src="..\ZuriIMAGES\img41.jpg" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

             <?php  
                $query="SELECT * FROM beauty where id=4572";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>

                <div class="shop-item">
                     <span class="shop-item-title"><?php echo $name;?></span>
                      <img src="..\ZuriIMAGES\img42.jpg" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span> 
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

                 <?php  
                $query="SELECT * FROM beauty where id=4573";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>

                <div class="shop-item">
                     <span class="shop-item-title"><?php echo $name;?></span>
                      <img src="..\ZuriIMAGES\img43.jpg" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

                 <?php  
                $query="SELECT * FROM beauty where id=4574";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>

                <div class="shop-item">
                    <br><br>
                    <span class="shop-item-title">Curlers, combs & Tongs<br><?php echo $name;?></span>
                     <img src="..\ZuriIMAGES\img37.jpg" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

                <?php  
                $query="SELECT * FROM beauty where id=4575";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>

                <div class="shop-item">
                    <span class="shop-item-title"><br><br><?php echo $name;?></span>
                     <img src="..\ZuriIMAGES\img6.jpg" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

                <?php  
                $query="SELECT * FROM beauty where id=4576";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>

                <div class="shop-item">
                    <span class="shop-item-title"><br><br><?php echo $name;?></span>
                     <img src="..\ZuriIMAGES\img7.jpg" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button"type="button">ADD TO CART</button>
                    </div>
                </div>

                <?php  
                $query="SELECT * FROM beauty where id=4577";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>

                <div class="shop-item">
                    <span class="shop-item-title"><br><br><?php echo $name;?></span>
                     <img src="..\ZuriIMAGES\img45.jpg" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

                <?php  
                $query="SELECT * FROM beauty where id=4578";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>

                <div class="shop-item">
                    <span class="shop-item-title"><br><br><?php echo $name;?></span>
                     <img src="..\ZuriIMAGES\img46.jpg" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

                 <?php  
                $query="SELECT * FROM beauty where id=4579";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>

                <div class="shop-item">
                    <span class="shop-item-title"><br><br><?php echo $name;?></span>
                     <img src="..\ZuriIMAGES\img47.jpg" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

            </div>
        </section>
        <section class="container content-section">
            <h2 class="section-header">Lipstick_Galore</h2>

             <?php  
                $query="SELECT * FROM beauty where id=4580";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>

            <div class="shop-items">
                <div class="shop-item">
                   <span class="shop-item-title"><br><br><?php echo $name;?></span>
                    <img src="..\ZuriIMAGES\img53.jpg" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

            <?php  
                $query="SELECT * FROM beauty where id=4581";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>
            

                <div class="shop-item">
                    <span class="shop-item-title"><br><br><?php echo $name;?></span>
                     <img src="..\ZuriIMAGES\img51.jpg" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>
           

            <?php  
                $query="SELECT * FROM beauty where id=4582";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>
            

                <div class="shop-item">
                    <span class="shop-item-title"><br><br><?php echo $name;?></span>
                     <img src="..\ZuriIMAGES\img52.jpg" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>
        

            <?php  
                $query="SELECT * FROM beauty where id=4583";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>
            

                <div class="shop-item">
                    <span class="shop-item-title"><br><br><?php echo $name;?></span>
                    <img src="..\ZuriIMAGES\img55.jpg" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

                <?php  
                $query="SELECT * FROM beauty where id=4584";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>
            

                <div class="shop-item">
                    <span class="shop-item-title"><br><br><?php echo $name;?></span>
                    <img src="..\ZuriIMAGES\img56.jpg" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>


                <?php  
                $query="SELECT * FROM beauty where id=4585";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>
            

                <div class="shop-item">
                    <span class="shop-item-title"><br><br><?php echo $name;?></span>
                    <img src="..\ZuriIMAGES\img57.jpg" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' class='shop-item-image'/>;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

            </div>

        </section>
        <section class="container content-section">
            <h2 class="section-header">Receipt</h2>
            <div class="cart-row">
                <span class="cart-item cart-header cart-column">ITEM</span>
                <span class="cart-price cart-header cart-column">PRICE</span>
                
            </div>
            <div class="cart-items">
            </div>
            <div class="cart-total">
                <strong class="cart-total-title">Total</strong>
                <span class="cart-total-price">$0</span>
            </div>
            <a href="../shop.html">
            <button class="btn btn-primary btn-purchase" type="button">Continue to Checkout</button>
            </a>


        </section>


<form action="storephp2.php#Search" method="POST">
    <label>Search</label>
<input type="text" name="name" placeholder="Enter your name">
<input type="text" name="price" placeholder="Enter your price">
<input type="submit" name="search" value="Search">
</form>

<?php 

$sql = "SELECT * FROM beauty";
if($result1 = mysqli_query($connect, $sql)){
    if(mysqli_num_rows($result1) > 0){
        echo "<table>";
            echo "<tr>";
                // echo "<th>name</th>";
               
            echo "</tr>";
        while($row = mysqli_fetch_array($result1)){
            echo "<tr>";
                // echo "<td>" . $row['name'] . "</td>";
               
            echo "</tr>";
        }
        echo "</table>";
        // Free result set
        mysqli_free_result($result1);
    } else{
        echo "No records matching your query were found.";
    }
} else{
    echo "ERROR: Could not able to execute $sql. " . mysqli_error($connect);
}
 if ($nametxt=$row['name']) {
     echo $row['name'];
 }
 else{
    echo "string";
 }
// Close connection
mysqli_close($connect);
?>


        <footer class="main-footer">
            <div class="container main-footer-container">
                <h3 class="band-name">Cradle</h3>
                <ul class="nav footer-nav">
                    
                    
                    <li>
                        <a href="https://www.facebook.com" target="_blank">
                            <img src="Images/Facebook Logo.png">
                        </a>
                    </li>
                </ul>
            </div>
        </footer>
    </body>
</html>