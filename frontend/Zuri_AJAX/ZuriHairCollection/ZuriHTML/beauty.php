<?php
$connect=mysqli_connect("localhost","root","","e-commerce");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
 }

?>

<!DOCTYPE html>
<html>
<head>
  <title>Zuri_Beauty</title>
   <!-- Metas -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Links -->
  <link rel="stylesheet" type="text/css" href="..\ZuriCSS\ZuriCSS.css"><meta charset="UTF-8">
  <link rel="stylesheet" href="..\Bootstrap_Code\bCSS.css">
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> -->
  <link href="..\ZuriCSS\Font_Pacifico.css" rel="stylesheet"> 
  <link href="..\ZuriCSS\W3CSS.css" rel="stylesheet">
  <link href="..\ZuriCSS\Font_JosefinSans.css" rel="stylesheet"> 
 <!--  <link rel="stylesheet" href="..\ZuriCSS\socailmediaicons.css">   -->

<style type="text/css">
   /* Add a dark background color with a little bit see-through */
.navbar {
  margin-bottom: 0;
  background-color: #2d2d30;
  border: 0;
  font-size: 11px !important;
  letter-spacing: 4px;
  opacity: 0.9;
  padding-bottom: 0;
  height: 70px;
}

/* Add a gray color to all navbar links */
.navbar li a, .navbar .navbar-brand {
  color: #d5d5d5 !important;
}

/* On hover, the links will turn white */
.navbar-nav li a:hover {
  color: #fff !important;
}

/* The active link */
.navbar-nav li.active a {
  color: #fff !important;
  background-color:#29292c !important;
}

/* Remove border color from the collapsible button */
.navbar-default .navbar-toggle {
  border-color: transparent;
}
.sidenav {
  height: 1605px;
  width: 200px;
  position: fixed;
  z-index: 1;
  /*top: 0;
  left: 0;*/
 margin-top: 60px;
  background-color: #111;
  overflow-x: hidden;
  padding-top: 5px;
  margin-top: 5px;
}

/* Style the sidenav links and the dropdown button */
.sidenav a, .dropdown-btn {
  padding: 6px 8px 6px 16px;
  text-decoration: none;
  font-size: 20px;
  color: #818181;
  display: block;
  border: none;
  background: none;
  width: 100%;
  text-align: left;
  cursor: pointer;
  outline: none;
}

/* On mouse-over */
.sidenav a:hover, .dropdown-btn:hover {
  color: #f1f1f1;
}

/* Main content */
.main {
  margin-left: 200px; /* Same as the width of the sidenav */
  font-size: 20px; /* Increased text to enable scrolling */
  padding: 0px 10px;
}

/* Add an active class to the active dropdown button */
.active {
  background-color: #6495ED;
  color: white;
}

/* Dropdown container (hidden by default). Optional: add a lighter background color and some left padding to change the design of the dropdown content */
.dropdown-container {
  display: none;
  background-color: #262626;
  padding-left: 8px;
}

/* Optional: Style the caret down icon */
.fa-caret-down {
  float: right;
  padding-right: 8px;
}

/* Some media queries for responsiveness */
@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}

.fa {
  /*padding: 10px;*/
  font-size: 30px;
  /*width: 50px;*/
  /*text-align: center;*/
  text-decoration: none;
  /*margin: 5px 2px;*/
}


.fa:hover{
    opacity: 0.7;
}

.fa-facebook {
  background: #3B5998;
  color: white;
}

.fa-twitter {
  background: #55ACEE;
  color: white;
}

.fa-instagram {
  background: #125688;
  color: white;
}

.fa-pinterest {
  background: #cb2027;
  color: white;
}

.fa-snapchat-ghost {
  background: #fffc00;
  color: white;
  text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
}

</style> 
</head>
<body>
   
  <!-- Navigation bar -->
<div id="header">
  <nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
    
      <span class="navbar-brand" style="font-family: 'Pacifico', cursive; font-size: 50px; ">Zuri Hair Collection</span>
      
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
      <li><a href="Sign_up.html" class="btn-btn-success" style="font-size:12px;">Signing up?</a></li>
      <li><a href="Login_Page.html" class="btn-btn-success" style="font-size:12px;">Logging in?</a></li>
       <li><a href="..\ZuriHTML\Home_real_v2.html">HOME</a></li>
        <li><a href="..\ZuriHTML\Zuri_Hair.html">HAIR</a></li>
        <li><a href="..\ZuriHTML\beauty.php">BEAUTY</a></li>
        <li><a href="..\ZuriHTML\Zuri_HairCare.html">HAIR CARE</a></li>
      </ul>
     
    </div>
   
</div>
  </div>
</nav>
</div>
<!--  End of navigation bar/header -->

<!-- Start of main body with the columns -->
<!-- Column 1 -->
  <div id="bodyleft">
    <div class="sidenav">
  <!-- <a href="#about">About</a>
  <a href="#services">Services</a>
  <a href="#clients">Clients</a>
  <a href="#contact">Contact</a> -->
  <p class="dropdown-btn">Click to see submenus</p>
  <button class="dropdown-btn">Zuri_Hair
    <i class="fa fa-caret-down"></i>
  </button>
  <div class="dropdown-container">
    <a href="..\ZuriHTML\Zuri_Hair.html#Human-hair">Human-hair</a>
    <a href="..\ZuriHTML\Zuri_Hair.html#Braids">Braids</a>
    <a href="..\ZuriHTML\Zuri_Hair.html#Lacefront">Lacefront</a>
  </div>

   <button class="dropdown-btn">Zuri_Beauty
    <i class="fa fa-caret-down"></i>
  </button>
  <div class="dropdown-container">
    <a href="..\ZuriHTML\beauty.php#Make up brushes">Make-up brushes</a>
    <a href="..\ZuriHTML\beauty.php#Curlers & Tongs">Curlers & Tongs</a>
    <a href="..\ZuriHTML\beauty.php#Lipsticks">Lipsticks</a>
  </div>
  
  <button class="dropdown-btn">Zuri_Hair_Care 
    <i class="fa fa-caret-down"></i>
  </button>
  <div class="dropdown-container">
    <a href="..\ZuriHTML\Zuri_HairCare.html#Stylers">Stylers</a>
    <a href="..\ZuriHTML\Zuri_HairCare.html#Scalp products">Scalp products</a>
    <a href=..\ZuriHTML\Zuri_HairCare.html#Hairsprays">Hair sprays</a>
  </div>
  
<br><br>
  <div style="padding-left:5px; padding-right:5px;"><img src="..\ZuriIMAGES\hair.jpg" width="190px" height="280px"></div>
  
</div>

<script>
/* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
  this.classList.toggle("active");
  var dropdownContent = this.nextElementSibling;
  if (dropdownContent.style.display === "block") {
  dropdownContent.style.display = "none";
  } else {
  dropdownContent.style.display = "block";
  }
  });
}
</script>


  </div>

<!-- Column 2 -->
  <div id="bodycenter" style="padding-top:5px; margin-top: 5px; padding-left:10px; height:1600px; font-size: 30px;">
  <table>
    <th style="font-family: jokerman;" id="Make up brushes">Make up brushes</th>
      <tr>
        <td>
         <?php

    $query="SELECT * FROM beauty where id=4568";
    $result=mysqli_query($connect,$query);
        
  while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
  $image=$row['image'];
  $name=$row['name'];
  $price=$row['price'];
  $id=$row['id'];
  echo "<img src='".$image. "' alt='' height='200' width='250' /> ";
}
?>
          <blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><?php echo "Ksh ".$price;?><blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><blockquote style="text-align: center;"><?php echo $name;?></blockquote>
         <form method="post" action="http://localhost/ZuriHairCollection/ZuriHTML/trial_shoppingcart.php?action=add&id=<?php echo $id;?>"> 
          <input type="text" name="quantity" class="form-control"  placeholder="Enter desired quantity">
        <input type="hidden" name="hidden_name" value="<?php echo $name; ?>">
        <input type="hidden" name="hidden_price" value="<?php echo $price; ?>">
          <input type="submit" name="add1" value="ADD TO CART" style="margin-top: 5px;" class="btn-btn-success"> 
        </form>
        </td>


        <td>
         <?php

    $query="SELECT * FROM beauty where id=4569";
    $result=mysqli_query($connect,$query);
        
  while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
  $image=$row['image'];
  $name=$row['name'];
  $price=$row['price'];
  $id=$row['id'];
  echo "<img src='".$image. "' alt='' height='200' width='250' /> ";
}
?>
          <blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><?php echo "Ksh ".$price;?><blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><blockquote style="text-align: center;"><?php echo $name;?></blockquote>
         <form method="post" action="http://localhost/ZuriHairCollection/ZuriHTML/trial_shoppingcart.php?action=add&id=<?php echo $id;?>"> 
          <input type="text" name="quantity" class="form-control"  placeholder="Enter desired quantity">
        <input type="hidden" name="hidden_name" value="<?php echo $name; ?>">
        <input type="hidden" name="hidden_price" value="<?php echo $price; ?>">
          <input type="submit" name="add1" value="ADD TO CART" style="margin-top: 5px;" class="btn-btn-success"> 
        </form>
       </td>


       <td>
         <?php

    $query="SELECT * FROM beauty where id=4570";
    $result=mysqli_query($connect,$query);
        
  while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
  $image=$row['image'];
  $name=$row['name'];
  $price=$row['price'];
  $id=$row['id'];
  echo "<img src='".$image. "' alt='' height='200' width='250' /> ";
}
?>
          <blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><?php echo "Ksh ".$price;?><blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><blockquote style="text-align: center;"><?php echo $name;?></blockquote>
         <form method="post" action="http://localhost/ZuriHairCollection/ZuriHTML/trial_shoppingcart.php?action=add&id=<?php echo $id;?>"> 
          <input type="text" name="quantity" class="form-control"  placeholder="Enter desired quantity">
        <input type="hidden" name="hidden_name" value="<?php echo $name; ?>">
        <input type="hidden" name="hidden_price" value="<?php echo $price; ?>">
          <input type="submit" name="add1" value="ADD TO CART" style="margin-top: 5px;" class="btn-btn-success"> 
        </form>
      </td>

    </tr>



      <th style="padding-top: 20px; font-family: harrington;">Makeup brushes cont..</th>
      <tr>
        <td>
         <?php

    $query="SELECT * FROM beauty where id=4571";
    $result=mysqli_query($connect,$query);
        
  while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
  $image=$row['image'];
  $name=$row['name'];
  $price=$row['price'];
  $id=$row['id'];
  echo "<img src='".$image. "' alt='' height='200' width='250' /> ";
}
?>
          <blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><?php echo "Ksh ".$price;?><blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><blockquote style="text-align: center;"><?php echo $name;?></blockquote>
         <form method="post" action="http://localhost/ZuriHairCollection/ZuriHTML/trial_shoppingcart.php?action=add&id=<?php echo $id;?>"> 
          <input type="text" name="quantity" class="form-control"  placeholder="Enter desired quantity">
        <input type="hidden" name="hidden_name" value="<?php echo $name; ?>">
        <input type="hidden" name="hidden_price" value="<?php echo $price; ?>">
          <input type="submit" name="add1" value="ADD TO CART" style="margin-top: 5px;" class="btn-btn-success"> 
        </form>
      </td>



        <td>
         <?php

    $query="SELECT * FROM beauty where id=4572";
    $result=mysqli_query($connect,$query);
        
  while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
  $image=$row['image'];
  $name=$row['name'];
  $price=$row['price'];
  $id=$row['id'];
  echo "<img src='".$image. "' alt='' height='200' width='250' /> ";
}
?>
          <blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><?php echo "Ksh ".$price;?><blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><blockquote style="text-align: center;"><?php echo $name;?></blockquote>
         <form method="post" action="http://localhost/ZuriHairCollection/ZuriHTML/trial_shoppingcart.php?action=add&id=<?php echo $id;?>"> 
          <input type="text" name="quantity" class="form-control"  placeholder="Enter desired quantity">
        <input type="hidden" name="hidden_name" value="<?php echo $name; ?>">
        <input type="hidden" name="hidden_price" value="<?php echo $price; ?>">
          <input type="submit" name="add1" value="ADD TO CART" style="margin-top: 5px;" class="btn-btn-success"> 
        </form></td>
       
      </tr> 
      <th style="padding-top: 30px; font-family:script mt bold" id="Curlers & Tongs">Curlers, combs & Tongs</th>
      <tr>
        <td>
         <?php

    $query="SELECT * FROM beauty where id=4573";
    $result=mysqli_query($connect,$query);
        
  while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
  $image=$row['image'];
  $name=$row['name'];
  $price=$row['price'];
  $id=$row['id'];
  echo "<img src='".$image. "' alt='' height='200' width='250' /> ";
}
?>
          <blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><?php echo "Ksh ".$price;?><blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><blockquote style="text-align: center;"><?php echo $name;?></blockquote>
         <form method="post" action="http://localhost/ZuriHairCollection/ZuriHTML/trial_shoppingcart.php?action=add&id=<?php echo $id;?>"> 
          <input type="text" name="quantity" class="form-control"  placeholder="Enter desired quantity">
        <input type="hidden" name="hidden_name" value="<?php echo $name; ?>">
        <input type="hidden" name="hidden_price" value="<?php echo $price; ?>">
          <input type="submit" name="add1" value="ADD TO CART" style="margin-top: 5px;" class="btn-btn-success"> 
        </form></td>
       


        <td>
         <?php

    $query="SELECT * FROM beauty where id=4574";
    $result=mysqli_query($connect,$query);
        
  while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
  $image=$row['image'];
  $name=$row['name'];
  $price=$row['price'];
  $id=$row['id'];
  echo "<img src='".$image. "' alt='' height='200' width='250' /> ";
}
?>
          <blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><?php echo "Ksh ".$price;?><blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><blockquote style="text-align: center;"><?php echo $name;?></blockquote>
         <form method="post" action="http://localhost/ZuriHairCollection/ZuriHTML/trial_shoppingcart.php?action=add&id=<?php echo $id;?>"> 
          <input type="text" name="quantity" class="form-control"  placeholder="Enter desired quantity">
        <input type="hidden" name="hidden_name" value="<?php echo $name; ?>">
        <input type="hidden" name="hidden_price" value="<?php echo $price; ?>">
          <input type="submit" name="add1" value="ADD TO CART" style="margin-top: 5px;" class="btn-btn-success"> 
        </form></td>
       
        

        <td>
         <?php

    $query="SELECT * FROM beauty where id=4575";
    $result=mysqli_query($connect,$query);
        
  while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
  $image=$row['image'];
  $name=$row['name'];
  $price=$row['price'];
  $id=$row['id'];
  echo "<img src='".$image. "' alt='' height='200' width='250' /> ";
}
?>
          <blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><?php echo "Ksh ".$price;?><blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><blockquote style="text-align: center;"><?php echo $name;?></blockquote>
         <form method="post" action="http://localhost/ZuriHairCollection/ZuriHTML/trial_shoppingcart.php?action=add&id=<?php echo $id;?>"> 
          <input type="text" name="quantity" class="form-control"  placeholder="Enter desired quantity">
        <input type="hidden" name="hidden_name" value="<?php echo $name; ?>">
        <input type="hidden" name="hidden_price" value="<?php echo $price; ?>">
          <input type="submit" name="add1" value="ADD TO CART" style="margin-top: 5px;" class="btn-btn-success"> 
        </form></td>
       
       
      </tr> 
  </table>
</div>

<div id="bodycenter" style="padding-top:5px; margin-top: 5px; padding-left:10px; height:1600px; font-size: 30px;"><br>
  <table>
    <th style="font-family: century gothic; font-size: 20px;">Curlers, combs & tongs cont..</th>
      <tr>
       <td>
         <?php

    $query="SELECT * FROM beauty where id=4576";
    $result=mysqli_query($connect,$query);
        
  while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
  $image=$row['image'];
  $name=$row['name'];
  $price=$row['price'];
  $id=$row['id'];
  echo "<img src='".$image. "' alt='' height='200' width='250' /> ";
}
?>
          <blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><?php echo "Ksh ".$price;?><blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><blockquote style="text-align: center;"><?php echo $name;?></blockquote>
         <form method="post" action="http://localhost/ZuriHairCollection/ZuriHTML/trial_shoppingcart.php?action=add&id=<?php echo $id;?>"> 
          <input type="text" name="quantity" class="form-control"  placeholder="Enter desired quantity">
        <input type="hidden" name="hidden_name" value="<?php echo $name; ?>">
        <input type="hidden" name="hidden_price" value="<?php echo $price; ?>">
          <input type="submit" name="add1" value="ADD TO CART" style="margin-top: 5px;" class="btn-btn-success"> 
        </form></td>
       

        <td>
         <?php

    $query="SELECT * FROM beauty where id=4577";
    $result=mysqli_query($connect,$query);
        
  while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
  $image=$row['image'];
  $name=$row['name'];
  $price=$row['price'];
  $id=$row['id'];
  echo "<img src='".$image. "' alt='' height='200' width='250' /> ";
}
?>
          <blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><?php echo "Ksh ".$price;?><blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><blockquote style="text-align: center;"><?php echo $name;?></blockquote>
         <form method="post" action="http://localhost/ZuriHairCollection/ZuriHTML/trial_shoppingcart.php?action=add&id=<?php echo $id;?>"> 
          <input type="text" name="quantity" class="form-control"  placeholder="Enter desired quantity">
        <input type="hidden" name="hidden_name" value="<?php echo $name; ?>">
        <input type="hidden" name="hidden_price" value="<?php echo $price; ?>">
          <input type="submit" name="add1" value="ADD TO CART" style="margin-top: 5px;" class="btn-btn-success"> 
        </form></td>
       


       <td>
         <?php

    $query="SELECT * FROM beauty where id=4578";
    $result=mysqli_query($connect,$query);
        
  while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
  $image=$row['image'];
  $name=$row['name'];
  $price=$row['price'];
  $id=$row['id'];
  echo "<img src='".$image. "' alt='' height='200' width='250' /> ";
}
?>
          <blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><?php echo "Ksh ".$price;?><blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><blockquote style="text-align: center;"><?php echo $name;?></blockquote>
         <form method="post" action="http://localhost/ZuriHairCollection/ZuriHTML/trial_shoppingcart.php?action=add&id=<?php echo $id;?>"> 
          <input type="text" name="quantity" class="form-control"  placeholder="Enter desired quantity">
        <input type="hidden" name="hidden_name" value="<?php echo $name; ?>">
        <input type="hidden" name="hidden_price" value="<?php echo $price; ?>">
          <input type="submit" name="add1" value="ADD TO CART" style="margin-top: 5px;" class="btn-btn-success"> 
        </form></td>
       
       
      </tr>
      <th style="padding-top: 20px; font-family: jokerman;">Gloss anyone?</th>
      <tr>
        <td>
         <?php

    $query="SELECT * FROM beauty where id=4579";
    $result=mysqli_query($connect,$query);
        
  while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
  $image=$row['image'];
  $name=$row['name'];
  $price=$row['price'];
  $id=$row['id'];
  echo "<img src='".$image. "' alt='' height='200' width='250' /> ";
}
?>
          <blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><?php echo "Ksh ".$price;?><blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><blockquote style="text-align: center;"><?php echo $name;?></blockquote>
         <form method="post" action="http://localhost/ZuriHairCollection/ZuriHTML/trial_shoppingcart.php?action=add&id=<?php echo $id;?>"> 
          <input type="text" name="quantity" class="form-control"  placeholder="Enter desired quantity">
        <input type="hidden" name="hidden_name" value="<?php echo $name; ?>">
        <input type="hidden" name="hidden_price" value="<?php echo $price; ?>">
          <input type="submit" name="add1" value="ADD TO CART" style="margin-top: 5px;" class="btn-btn-success"> 
        </form></td>
       

        <td>
         <?php

    $query="SELECT * FROM beauty where id=4580";
    $result=mysqli_query($connect,$query);
        
  while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
  $image=$row['image'];
  $name=$row['name'];
  $price=$row['price'];
  $id=$row['id'];
  echo "<img src='".$image. "' alt='' height='200' width='250' /> ";
}
?>
          <blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><?php echo "Ksh ".$price;?><blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><blockquote style="text-align: center;"><?php echo $name;?></blockquote>
         <form method="post" action="http://localhost/ZuriHairCollection/ZuriHTML/trial_shoppingcart.php?action=add&id=<?php echo $id;?>"> 
          <input type="text" name="quantity" class="form-control"  placeholder="Enter desired quantity">
        <input type="hidden" name="hidden_name" value="<?php echo $name; ?>">
        <input type="hidden" name="hidden_price" value="<?php echo $price; ?>">
          <input type="submit" name="add1" value="ADD TO CART" style="margin-top: 5px;" class="btn-btn-success"> 
        </form></td>
       
       
      </tr> 
      <th style="padding-top: 30px; font-family: harrington;" id="Lipsticks">Lipstick galore</th>
      <tr>
        <td>
         <?php

    $query="SELECT * FROM beauty where id=4581";
    $result=mysqli_query($connect,$query);
        
  while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
  $image=$row['image'];
  $name=$row['name'];
  $price=$row['price'];
  $id=$row['id'];
  echo "<img src='".$image. "' alt='' height='200' width='250' /> ";
}
?>
          <blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><?php echo "Ksh ".$price;?><blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><blockquote style="text-align: center;"><?php echo $name;?></blockquote>
         <form method="post" action="http://localhost/ZuriHairCollection/ZuriHTML/trial_shoppingcart.php?action=add&id=<?php echo $id;?>"> 
          <input type="text" name="quantity" class="form-control"  placeholder="Enter desired quantity">
        <input type="hidden" name="hidden_name" value="<?php echo $name; ?>">
        <input type="hidden" name="hidden_price" value="<?php echo $price; ?>">
          <input type="submit" name="add1" value="ADD TO CART" style="margin-top: 5px;" class="btn-btn-success"> 
        </form></td>
       
        <td>
         <?php

    $query="SELECT * FROM beauty where id=4582";
    $result=mysqli_query($connect,$query);
        
  while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
  $image=$row['image'];
  $name=$row['name'];
  $price=$row['price'];
  $id=$row['id'];
  echo "<img src='".$image. "' alt='' height='200' width='250' /> ";
}
?>
          <blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><?php echo "Ksh ".$price;?><blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><blockquote style="text-align: center;"><?php echo $name;?></blockquote>
         <form method="post" action="http://localhost/ZuriHairCollection/ZuriHTML/trial_shoppingcart.php?action=add&id=<?php echo $id;?>"> 
          <input type="text" name="quantity" class="form-control"  placeholder="Enter desired quantity">
        <input type="hidden" name="hidden_name" value="<?php echo $name; ?>">
        <input type="hidden" name="hidden_price" value="<?php echo $price; ?>">
          <input type="submit" name="add1" value="ADD TO CART" style="margin-top: 5px;" class="btn-btn-success"> 
        </form></td>
       

        <td>
         <?php

    $query="SELECT * FROM beauty where id=4583";
    $result=mysqli_query($connect,$query);
        
  while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
  $image=$row['image'];
  $name=$row['name'];
  $price=$row['price'];
  $id=$row['id'];
  echo "<img src='".$image. "' alt='' height='200' width='250' /> ";
}
?>
          <blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><?php echo "Ksh ".$price;?><blockquote style="font-family: 'Josefin Sans', sans-serif; font-size: 18px;"><blockquote style="text-align: center;"><?php echo $name;?></blockquote>
         <form method="post" action="http://localhost/ZuriHairCollection/ZuriHTML/trial_shoppingcart.php?action=add&id=<?php echo $id;?>"> 
          <input type="text" name="quantity" class="form-control"  placeholder="Enter desired quantity">
        <input type="hidden" name="hidden_name" value="<?php echo $name; ?>">
        <input type="hidden" name="hidden_price" value="<?php echo $price; ?>">
          <input type="submit" name="add1" value="ADD TO CART" style="margin-top: 5px;" class="btn-btn-success"> 
        </form></td>
       
      </tr> 
  </table>
</div>
<!-- Column 3 -->
  <div id="bodyright">
  
  </div>
  

<div id="footer" style="font-size: 20px;">
  <p>Like and share!!</p>
  <a href="#" class="fa fa-facebook"><img src="..\ZuriIMAGES\Social Media\fb.jpg" width="100px" height="80px"></a>
  <a href="#" class="fa fa-twitter"><img src="..\ZuriIMAGES\Social Media\t.jpg" width="100px" height="80px"></a>
  <a href="#" class="fa fa-instagram"><img src="..\ZuriIMAGES\Social Media\instagram2.jpg" width="100px" height="80px"></a>
<a href="#" class="fa fa-pinterest"><img src="..\ZuriIMAGES\Social Media\pinterest.jpg" width="100px" height="80px"></a>
<a href="#" class="fa fa-snapchat-ghost"><img src="..\ZuriIMAGES\Social Media\snapchat.png" width="100px" height="80px"></a>
<br>
<hr style="margin-right: 30%; margin-left:30%;">

Contact us on:
<div>
<p style="border-right: 2px; border-color: white;">0765743575</p>
<p style="border-right: 2px; border-color: white;">zurihair.com</p>
<p style="border-right: 2px; border-color: white;">zurihair@gmail.com</p>
</div>
<br>
<br>
Payment Methods
<div>
 <span class="fa fa-facebook"><img src="..\ZuriIMAGES\mpesa.jpg" width="100px" height="80px"></span>
 <span class="fa fa-facebook"><img src="..\ZuriIMAGES\paypal.jpg" width="100px" height="80px"></span>
 <span class="fa fa-facebook"><img src="..\ZuriIMAGES\visa.jpg" width="100px" height="80px"></span>
 <span class="fa fa-facebook"><img src="..\ZuriIMAGES\mastercard.png" width="100px" height="80px"></span>
 </div> 
</div>
</body>
</html>
<?php
?>