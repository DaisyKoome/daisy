<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dogs</title>


    <style>
#customers {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #0070DE;
  color: white;
}
</style>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>



    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item active">
                    <a class="nav-link" href="purchase.html.php">Home<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="services.html">Our Services</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="aboutUs.html">About Us</a>
                </li>
            </ul>

            <button class="btn btn-outline-info my-2 my-sm-0" type="button" data-toggle="modal" data-target="#exampleModal" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-shopping-cart fa_custom fa-1x"></i>Shopping Cart</button>
            <button class="btn btn-outline-info my-2 my-sm-0" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Search</button>
            <?php session_start();
            if(isset($_SESSION['name'])){
              $name = $_SESSION['name'];
              ?>
                <a href="logOut.php" class="btn btn-outline-info my-2 my-sm-0" ><span class="badge badge-pill badge-light"><?php echo $name;?></span>Log Out</a>
              <?php
            }
            else{?>
            <a href="clientLogIn.html.php" class="btn btn-outline-info my-2 my-sm-0" >Log In</a>
            <?php }?>
        </div>
    </nav>

<br>


<?php
error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
session_start();
if(isset($_POST['add_to_cart'])){
  if(isset($_SESSION["shopping_cart"]))
  {
    $itemArray_ID = array_column($_SESSION["shopping_cart"],"id");
    if(!in_array($_POST['id'],$itemArray_ID)){
      $count = count($_SESSION["shopping_cart"]); 
      $itemArray = array (
        'id' =>$_POST['id'],
        'breedC' => $_POST['breedC'],
        'genderC' => $_POST['genderC'],
        'priceC' => $_POST['priceC'],
        'ageC' => $_POST['ageC']
    );
    $_SESSION["shopping_cart"][$count] = $itemArray;
    // echo $count;
    }
    else
    {
      echo '<script>alert("Item already added to cart")</script>';
    }
   
 
  }
  else {
    $itemArray = array (
        'id' =>$_POST['id'],
        'breedC' => $_POST['breedC'],
        'genderC' => $_POST['genderC'],
        'priceC' => $_POST['priceC'],
        'ageC' => $_POST['ageC']
    );
   $_SESSION["shopping_cart"][0] = $itemArray;
    
  }
  

}
?>


<div class="collapse" id="collapseExample">
  <div class="card card-body">
   

              <form method="POST">
                  <center>
                  <div class="row">
                      
                      <div class="col-md-3">
                            <input type="text" name="age" placeholder="Enter Age">
                      </div>
                      <div class="col-md-2">
                            <input type="text" name="breed" placeholder="Enter Breed">
                      </div>
                      <div class="col-md-3">
                       
                  
                     <span>Gender</span><br>
                     <div class="custom-control custom-radio">
                        <input type="radio" value="M" id="customRadio1" name="gender" class="custom-control-input">
                        <label class="custom-control-label" for="customRadio1">Male</label>
                      </div>
                      <div class="custom-control custom-radio">
                        <input type="radio" value="F" id="customRadio2" name="gender" class="custom-control-input">
                        <label class="custom-control-label" for="customRadio2">Female</label>
                      </div>
                      
                      </div>
                      <div class="col-md-2">
                      <!-- <input type="range" min="0" max =  "1000000" scale="10000"> -->
                            <input type="number" placeholder="Max Price" name="max">
                      </div>      
                      <div class="col-md-2">                    
                            <input type="number" placeholder="Min Price" name="min">
                      </div>
                     
                  </div>
               <br>
                  
               <input class="btn btn-outline-info my-2 my-sm-0" type="submit" name="search" value="Go">
                </center>
              </form>
  </div>
</div>

              <br>

<?php
if(isset($_GET["action"])){
  if ($_GET["action"] === "delete"){
    foreach ($_SESSION["shopping_cart"] as $number => $value) {
      if(!empty($_GET['id'])){
      if ($value['id']==$_GET['id']) {
        unset($_SESSION['shopping_cart'][$number]);
        echo '<script>alert("Item Removed ")</script>';
        unset($_GET['id']);
        unset($_GET['action']);
      }
    }
    }
  }
}

?>

<!-- ===================printing the cart -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cart Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table id="customers">
  <tr>
    <th>Breed</th>
    <th>Gender</th>
    <th>Price</th>
    <th>Age(months)</th>
  </tr>  
<?php
$total= 0;
foreach($_SESSION["shopping_cart"] as $number => $values){
  ?>
  <tr>
      <td><?php echo $values['breedC']; ?></td>  
      <td><?php echo $values['genderC']; ?></td>  
      <td><?php echo $values['priceC']; ?></td>  
      <td><?php echo $values['ageC']; ?></td>  
      <td><a href="index.php?action=delete&id=<?php echo $values['id'];?>" class="btn btn-danger btn-sm">Remove</a></td>
      
  </tr>
  <?
  $total = $total + $values['priceC'];
}
?>
<td><b>Total : <?php echo number_format($total); ?></b></td>
</table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Continue Shopping</button>
        <a href="logInRedirectTrasactions.php" type="button" class="btn btn-primary" <?php if($total==0){echo"hidden";}?>>Purchase</a>
      </div>
    </div>
  </div>
</div>


<!-- ===================printing the cart  =======    #end--> 





<?php
require 'Includes/db.connect.php';

$age = (empty($_POST['age'])?"":$_POST['age']);
$gender = (empty($_POST['gender'])?"":$_POST['gender']);
$breed = (empty($_POST['breed'])?"":$_POST['breed']);
$max = (empty($_POST['max'])?0:$_POST['max']);
$min = (empty($_POST['min'])?0:$_POST['min']);

// echo $min . "  " .$max;
if (empty($_POST['search'])) {
$result = mysqli_query($key,"SELECT * FROM `Dogs` where`status` like 'A'");

mysqli_close($key);
}
// ===============Wild Card  Section=======================
else{

  
// $age = ($age==""?"%":$age);
if ($age=="") {
  $age='%';
}
if ($gender=="") {
  $gender ='%';
}
if ($breed=="") {
 $breed='%';
}

if ($min===0 && $max===0) {
  $result = mysqli_query($key,"SELECT * FROM `Dogs` WHERE  `age` like '$age' && `sex`like '$gender' &&  `breed`like'$breed' && `status` like 'A'"); 
 
  // echo "One";
}
elseif ($max!=0 && $min===0) {
  $result = mysqli_query($key,"SELECT * FROM `Dogs` WHERE `age` like'$age' && `sex`like '$gender' &&  `price` < $max && `breed`like '$breed'  && `status` like 'A'"); 
  // echo "Two";
}
else if ($min!=0 && $max===0) {
  $result = mysqli_query($key,"SELECT * FROM `Dogs` WHERE `age`like'$age' && `sex`like'$gender' && $min <`price`  && `breed`like'$breed'  && `status` like 'A'");
  // echo "Three";
}
else if($min!=0 && $max!=0){
    if ($min > $max) {
        echo "<center><br><br><br><br><h3>Lower Limit Can`t Be Greater Than Higher Limit!!!</h2></center>";
        mysqli_close($key);
        exit();
    }
  $sql = "SELECT * FROM `Dogs` WHERE `age`like'$age' && `sex`like'$gender' && `breed` like '$breed'  && `price` between $min  and $max  && `status` like 'A'" ;
$result = mysqli_query($key,$sql);
// echo "Four";
    } 

}     

// ===============Wild Card  Section=======================End



if ( !$result ) 
            { 
                echo("Error description: " . mysqli_error($key));
                mysqli_close($key);
            exit();
            }

$count  = mysqli_num_rows($result);
if ($count==0) {
echo "<center><br><br><br><br><h2>No Dog Meet Your Specifications!!</h2></center>";
exit();
}

while ($result){

 



$x=0;
$col=0;
echo "<div class='row' >";
while ($x<=$count){

 
if ($col<4){
       if ($rowcolms = mysqli_fetch_array($result)) {
         
       
       
      ?>
        <div class="col-md-3">
            <center>

                <div class="card text-white bg-dark mb-3" style="width: 18rem;">
                    <img class="card-img-top" src="<?php echo $rowcolms['photo'];?>" alt="Card image cap" style="height: 12rem;">

                    <div class="card-body">

                        <span class="card-text">
                                        Breed : <?php echo $rowcolms['breed'];?></span><br>
                        <span class="card-text">
                                        Price : ksh <?php echo $rowcolms['price'];?></span><br>
                        <span class="card-text">Age : <?php echo $rowcolms['age'];?> months </span><br>
                        <span class="card-text"> Gender : <?php echo $rowcolms['sex'];?></span>
                    </div>
                    <!--  <ul class="list-group list-group-flush">
                                        <li class="list-group-item"></li>

                                    </ul> -->
                <form method="POST">   
                  <input type="text" value="<?php echo $rowcolms['ID'];?>" hidden="" name="id">             
                    <input type="text" value="<?php echo $rowcolms['breed'];?>" hidden="" name="breedC">
                    <input type="text" value="<?php echo $rowcolms['sex'];?>" name="genderC" hidden>
                    <input type="text" value="<?php echo $rowcolms['price'];?>" name="priceC" hidden>
                    <input type="text" value="<?php echo $rowcolms['age'];?>" name="ageC" hidden>
                    <input type="submit" class="btn btn-info" name="add_to_cart"value="Add To Cart">
                    
               </form>

                </div>
            </center>
        </div>
   <?php
   $col++;
    }
    else{
        echo "</div>";
    exit;
   }
   }

    else{
      $x++;
      break;
    }
       }

echo "</div>";

}
echo "</div>";
mysqli_close($key);
exit();
?>
</body>
</html>