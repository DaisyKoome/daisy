<?php

$connect=mysqli_connect("localhost","root","","e-commerce");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
 }

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Cradle | Shop</title>
        <meta name="description" content="This is the description">
        <link rel="stylesheet" href="../styles.css" />
        <script src="../store.js" async></script>
        <!-- Links -->
  <link rel="stylesheet" type="text/css" href="..\ZuriCSS\ZuriCSS.css"><meta charset="UTF-8">
  <link rel="stylesheet" href="..\Bootstrap_Code\bCSS.css">
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> -->
  <link href="..\ZuriCSS\Font_Pacifico.css" rel="stylesheet"> 
  <link href="..\ZuriCSS\W3CSS.css" rel="stylesheet">
  <link href="..\ZuriCSS\Font_JosefinSans.css" rel="stylesheet"> 
 <!--  <link rel="stylesheet" href="..\ZuriCSS\socailmediaicons.css">   -->
 <style type="text/css">

    .navbar {
  margin-bottom: 0;
  background-color: #2d2d30;
  border: 0;
  font-size: 11px !important;
  letter-spacing: 4px;
  opacity: 0.9;
  padding-bottom: 0;
  height: 70px;
}

/* Add a gray color to all navbar links */
.navbar li a, .navbar .navbar-brand {
  color: #d5d5d5 !important;
}

/* On hover, the links will turn white */
.navbar-nav li a:hover {
  color: #fff !important;
}

/* The active link */
.navbar-nav li.active a {
  color: #fff !important;
  background-color:#29292c !important;
}

/* Remove border color from the collapsible button */
.navbar-default .navbar-toggle {
  border-color: transparent;
}
     /* Dropdown container (hidden by default). Optional: add a lighter background color and some left padding to change the design of the dropdown content */
.dropdown-container {
  display: none;
  background-color: #262626;
  padding-left: 8px;
}

/* Optional: Style the caret down icon */
.fa-caret-down {
  float: right;
  padding-right: 8px;
}

/* Some media queries for responsiveness */
@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}

.fa {
  /*padding: 10px;*/
  font-size: 30px;
  /*width: 50px;*/
  /*text-align: center;*/
  text-decoration: none;
  /*margin: 5px 2px;*/
}


.fa:hover{
    opacity: 0.7;
}
 </style>
    </head>
    <body>
        <div id="header">
    <nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
    
      <span class="navbar-brand" style="font-family: 'Pacifico', cursive; font-size: 50px; ">Zuri Hair Collection</span>
      
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
      <li><a href="Sign_up.html" class="btn-btn-success" style="font-size:12px;">Signing up?</a></li>
      <li><a href="Login_Page.html" class="btn-btn-success" style="font-size:12px;">Logging in?</a></li>
       <li><a href="..\ZuriHTML\Home_real_v2.html">HOME</a></li>
        <li><a href="..\ZuriHTML\Zuri_Hair.html">HAIR</a></li>
        <li><a href="..\ZuriHTML\beauty.php">BEAUTY</a></li>
        <li><a href="..\ZuriHTML\Zuri_HairCare.html">HAIR CARE</a></li>
      </ul>
     
    </div>
   
</div>
  </div>
</nav>
</div>
<!--  End of navigation bar/header -->



</header>

        

<div id="bodyleft">
        <div class="sidenav">
  <!-- <a href="#about">About</a>
  <a href="#services">Services</a>
  <a href="#clients">Clients</a>
  <a href="#contact">Contact</a> -->
  <p class="dropdown-btn">Click to see submenus</p>
  <button class="dropdown-btn">Zuri_Hair
    <i class="fa fa-caret-down"></i>
  </button>
  <div class="dropdown-container">
    <a href="..\ZuriHTML\Zuri_Hair.html#Human-hair">Human-hair</a><br>
    <a href="..\ZuriHTML\Zuri_Hair.html#Braids">Braids</a><br>
    <a href="..\ZuriHTML\Zuri_Hair.html#Lacefront">Lacefront</a>
  </div>
  <br>
   <button class="dropdown-btn">Zuri_Beauty
    <i class="fa fa-caret-down"></i>
  </button>
  <div class="dropdown-container">
    <a href="storephp.php#Make up brushes">Make up brushes</a><br>
    <a href="storephp.php#Curlers, combs & Tongs">Curlers, combs & Tongs</a><br>
    <a href="storephp.php#Lipstick_Galore">Lipstick_Galore</a>
  </div>
  <br>
  <button class="dropdown-btn">Zuri_Hair_Care 
    <i class="fa fa-caret-down"></i>
  </button>
  <div class="dropdown-container">
    <a href="..\ZuriHTML\Zuri_HairCare.html#Stylers">Stylers</a><br>
    <a href="..\ZuriHTML\Zuri_HairCare.html#Scalp products">Scalp products</a><br>
    <a href="..\ZuriHTML\Zuri_HairCare.html#Hairsprays">Hair sprays</a>
  </div>
  
  <script>
/* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
  this.classList.toggle("active");
  var dropdownContent = this.nextElementSibling;
  if (dropdownContent.style.display === "block") {
  dropdownContent.style.display = "none";
  } else {
  dropdownContent.style.display = "block";
  }
  });
}
</script>


   
        <section class="container content-section">
        	<?php  
        	$query="SELECT * FROM beauty where id=4586";
    		$result=mysqli_query($connect,$query);
        
  			while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
  			$image=$row['image'];
  			$name=$row['name'];
  			$price=$row['price'];
	  		}
	  		
  			?>

            <h2 class="section-header" style="font-family: Jokerman;" id="Human-hair">Human-hair</h2>
            <div class="shop-items">
                <div class="shop-item">
                    <span class="shop-item-title"><?php echo $name;?></span>
                     <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>
                <?php  
            $query="SELECT * FROM beauty where id=4587";
            $result=mysqli_query($connect,$query);
        
            while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
            $image=$row['image'];
            $name=$row['name'];
            $price=$row['price'];
            }
            
            ?>
                <div class="shop-item">
                    <span class="shop-item-title"><?php echo $name;?></span>
                     <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

            <?php  
            $query="SELECT * FROM beauty where id=4588";
            $result=mysqli_query($connect,$query);
        
            while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
            $image=$row['image'];
            $name=$row['name'];
            $price=$row['price'];
            }
            
            ?>

                <div class="shop-item">
                    <span class="shop-item-title"><?php echo $name;?></span>
                     <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

                <?php  
                $query="SELECT * FROM beauty where id=4589";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>

                <div class="shop-item">
                    <span class="shop-item-title"><?php echo $name;?></span>
                    <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

             <?php  
                $query="SELECT * FROM beauty where id=4590";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>

                <div class="shop-item">
                     <span class="shop-item-title"><?php echo $name;?></span>
                     <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span> 
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

                 <?php  
                $query="SELECT * FROM beauty where id=4591";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>

                <div class="shop-item">
                     <span class="shop-item-title"><?php echo $name;?></span>
                     <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

                 <?php  
                $query="SELECT * FROM beauty where id=4592";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>

                <div class="shop-item">
                    <br><br>
                    <span class="shop-item-title" style="font-family: Jokerman;" id="Curlers, combs & Tongs">Curlers, combs & Tongs<br><?php echo $name;?></span>
                     <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

                <?php  
                $query="SELECT * FROM beauty where id=4593";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>

                <div class="shop-item">
                    <span class="shop-item-title"><br><br><?php echo $name;?></span>
                     <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

                <?php  
                $query="SELECT * FROM beauty where id=4594";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>

                <div class="shop-item">
                    <span class="shop-item-title"><br><br><?php echo $name;?></span>
                     <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button"type="button">ADD TO CART</button>
                    </div>
                </div>

                <?php  
                $query="SELECT * FROM beauty where id=4595";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>

                <div class="shop-item">
                    <span class="shop-item-title"><br><br><?php echo $name;?></span>
                     <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

                <?php  
                $query="SELECT * FROM beauty where id=4596";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>

                <div class="shop-item">
                    <span class="shop-item-title"><br><br><?php echo $name;?></span>
                     <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

                 <?php  
                $query="SELECT * FROM beauty where id=4597";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>

                <div class="shop-item">
                    <span class="shop-item-title"><br><br><?php echo $name;?></span>
                     <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

            </div>
        </section>
        <section class="container content-section">
            <h2 class="section-header" style="font-family: Jokerman;" id="Lipstick_Galore">Lipstick_Galore</h2>

             <?php  
                $query="SELECT * FROM beauty where id=4598";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>

            <div class="shop-items">
                <div class="shop-item">
                   <span class="shop-item-title"><br><br><?php echo $name;?></span>
                   <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

            <?php  
                $query="SELECT * FROM beauty where id=4599";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>
            

                <div class="shop-item">
                    <span class="shop-item-title"><br><br><?php echo $name;?></span>
                    <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>
           

            <?php  
                $query="SELECT * FROM beauty where id=4600";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>
            

                <div class="shop-item">
                    <span class="shop-item-title"><br><br><?php echo $name;?></span>
                     <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>
        

            <?php  
                $query="SELECT * FROM beauty where id=4601";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>
            

                <div class="shop-item">
                    <span class="shop-item-title"><br><br><?php echo $name;?></span>
                   <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

                <?php  
                $query="SELECT * FROM beauty where id=4602";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>
            

                <div class="shop-item">
                    <span class="shop-item-title"><br><br><?php echo $name;?></span>
                   <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' />;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>


                <?php  
                $query="SELECT * FROM beauty where id=4603";
                $result=mysqli_query($connect,$query);
        
                while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                $image=$row['image'];
                $name=$row['name'];
                $price=$row['price'];
                }
            
                ?>
            

                <div class="shop-item">
                    <span class="shop-item-title"><br><br><?php echo $name;?></span>
                   <img src="<?php echo $image;?>" class="shop-item-image" height="10px;" width="20px" style="visibility: hidden;">
                    <span class="shop-item-image" ><?php echo "<img src='".$image. "' alt='' height='200' width='250' class='shop-item-image'/>;"?></span>
                    <div class="shop-item-details">
                        <span class="shop-item-price">$<?php echo $price?></span>
                        <button class="btn btn-primary shop-item-button" type="button">ADD TO CART</button>
                    </div>
                </div>

            </div>

        </section>
        <br><br>
        <section class="container content-section">
            <h2 class="section-header" style="font-family: Jokerman">Shopping Cart</h2>
            <div class="cart-row">
                <span class="cart-item cart-header cart-column">ITEM</span>
                <span class="cart-price cart-header cart-column">PRICE</span>
                <span class="cart-quantity cart-header cart-column">QUANTITY</span>
            </div>
            <div class="cart-items">
            </div>
            <div class="cart-total">
                <strong class="cart-total-title">Total</strong>
                <span class="cart-total-price">$0</span>
            </div>
            <a href="../shop.html">
            <button class="btn btn-primary btn-purchase" type="button">Continue to Checkout</button>
            </a>


        </section>
 <table>
    <tr>
  <td><a href="#" class="fa fa-facebook"><img src="..\ZuriIMAGES\Social Media\fb.jpg" width="100px" height="80px"></a></td>
  <td><a href="#" class="fa fa-twitter"><img src="..\ZuriIMAGES\Social Media\t.jpg" width="100px" height="80px"></a></td>
  <td><a href="#" class="fa fa-instagram"><img src="..\ZuriIMAGES\Social Media\instagram2.jpg" width="100px" height="80px"></a></td>
<td><a href="#" class="fa fa-pinterest"><img src="..\ZuriIMAGES\Social Media\pinterest.jpg" width="100px" height="80px"></a></td>
<td><a href="#" class="fa fa-snapchat-ghost"><img src="..\ZuriIMAGES\Social Media\snapchat.png" width="100px" height="80px"></a></td>
<br>
<hr style="margin-right: 30%; margin-left:30%;">
</tr>
</table>