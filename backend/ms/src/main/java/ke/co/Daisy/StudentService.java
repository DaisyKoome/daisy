package ke.co.Daisy;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.Router;

public class StudentService extends AbstractVerticle {
    @Override
    public void start() {

        Router r=Router.router(this.vertx);
        r.get("/news").handler(this::news);
        this.vertx.createHttpServer()
            .requestHandler(r::accept)
            .listen(8085);

    }

    private void news(RoutingContext rc){
        JsonObject obj = new JsonObject()
            .put("lord", "Jesus is Lord!!");
        rc.response().end(obj.encode());
    }
}