package ke.co.Daisy;

import java.beans.Transient;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import ke.co.Daisy.StudentService;

@RunWith(VertxUnitRunner.class)
public class StudentAddTest{
    
    private Vertx vertx;

    @Before
    public void start(TestContext tc){
        this.vertx=Vertx.vertx();
        this.vertx.deployVerticle(StudentService.class.getName(), tc.asyncAssertSuccess());
        //tc.asyncAssertSuccess());
    }

    @After
    public void close(TestContext tc){
        this.vertx.close(tc.asyncAssertSuccess());
    }

    @Test
    public void addStudentTest(TestContext tc){
        Async async=tc.async();
        this.vertx.createHttpClient()
            .get(8085,"localhost","/news")
            .handler(h -> {
                h.bodyHandler(b -> {
                    JsonObject body = b.toJsonObject();
                    tc.assertTrue(
                        body.getString("lord").equalsIgnoreCase("Jesus is Lord!!"), 
                        "Jesus is truly lord");
                    async.complete();
                });
            });
    }

        /*.handler(h->{
            h.bodyhandler(b ->{
                Jsonobject s=b.toJsonobject();
            }            
        });*/
    //}).end(/*{json}*/);
}
